import * as express from 'express';
import {createServer, Server} from 'http';
import * as SocketIO from 'socket.io';
import * as socketIOClient from 'socket.io-client';
import * as os from 'os';
import {ServerBase} from './serverBase';
import {Constant} from "./helper/constant";
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Event} from './enums/Event'

export class ServerSumResult implements ServerBase {

    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;
    private ioClient;
    private sumValue: number;
    private sumValueSubject: BehaviorSubject<any> = new BehaviorSubject<any>(this.sumValue);
    private sumValueObserver = this.sumValueSubject.asObservable();

    constructor() {
        this.initConfigServer();
        this.socketListen();
    }

    getApp(): express.Application {
        return this.app;
    }

    initConfigServer(): void {
        this.app = express();
        this.server = createServer(this.app);
        this.port = process.env.PORT || Constant.PORT_SUM_RESULT_SERVER;
        this.io = SocketIO(this.server);
    }

    socketListen(): void {
        this.server.listen(this.port, () => {
        });

        this.io.on(Event.CONNECT, (socket: any) => {
            this.io.emit(Event.JOIN_NEW_USER, socket.id);
            this.sumValueObserver.subscribe(sumValue => {
                this.io.emit(Event.SUM_RESULT, {ClientId: socket.id, Value: sumValue});
            });
            socket.on(Event.DISCONNECT, () => {
                this.io.emit(Event.LEAVE_USER, socket.id);
            });
        });

        this.listenSocketServerSum();
    }

    /**
     * Escucha el resultado de la sumatoria de los numeros enviados por el otro socket,
     * serverSum.ts, el cual esta emitiendo cada 1000 ms el resultado de la sumatoria.
     */
    listenSocketServerSum(): void {
        this.ioClient = socketIOClient(`http://${os.hostname()}:${Constant.PORT_SUM_SERVER}`);
        this.ioClient.on(Event.CONNECT, () => {
            this.ioClient.on(Event.SUM_VALID_NUMBER, (sumValue: number) => {
                this.sumValue = sumValue;
                this.sumValueSubject.next(this.sumValue);
            });
        });
    }
}
