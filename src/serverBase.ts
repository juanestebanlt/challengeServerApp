import * as express from 'express';

export interface ServerBase {
    /**
     * Inicializamos todas las configuraciones necesarias
     * para que el socket pueda funcionar de manera correcta
     * @constructor
     */
    initConfigServer(): void;

    /**
     * Configuramos el socket para que escuche todas las peticiones que se hagan
     * a traves del puerto que se le especifique al socket.
     * @constructor
     */
    socketListen(): void;

    /**
     *
     * @returns {express.Application}
     * @constructor
     */
    getApp(): express.Application;

}
