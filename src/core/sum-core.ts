export class SumCore {
    /**
     * Genera aleatoriamente numeros entre el min y la max
     * @param {number} minNumber, por defecto tiene 0
     * @param {number} maxNumber, por defecto tiene 20
     * @returns {number} return un numero entero entre el min y el max
     * @constructor
     */
    static GenerateRandomNumberInt(minNumber: number = 0, maxNumber: number = 20): number {
        const randomNumber: number = Math.random() * (maxNumber - minNumber) + minNumber;
        return Math.round(Math.floor(randomNumber));
    }

    /**
     * Suma dos numeros, siempre y cuando cumpla con la validacion {@link ValidIncomingData}
     * @param {number} numberOne
     * @param {number} numberTwo
     * @returns {number}, retorna la sumatoria de los dos numeros o 0 si no cumple con validacion previa
     * @constructor
     */
    static SumNumbers(numberOne: number, numberTwo: number): number {
        if (SumCore.ValidIncomingData(numberOne, numberTwo)) {
            return numberOne + numberTwo;
        }
        return 0;
    }

    /**
     * Regla de negocio para validar si cumple con los criterios de entrada del socket.
     * @param {number} numberOne
     * @param {number} numberTwo
     * @returns {boolean}, si cumple retorna True y si no False
     * @constructor
     */
    static ValidIncomingData(numberOne: number, numberTwo: number): boolean {
        return (numberOne >= 0 && numberOne <= 20)
            && (numberTwo >= 0 && numberTwo <= 20);

    }
}
