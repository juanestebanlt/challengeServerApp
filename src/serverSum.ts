import * as express from "express";
import {createServer, Server} from "http";
import * as SocketIO from "socket.io";
import {ServerBase} from "./serverBase";
import {SumCore} from "./core/sum-core";
import {Constant} from "./helper/constant";
import {Event} from './enums/Event'

export class ServerSum implements ServerBase {
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;

    constructor() {
        this.initConfigServer();
        this.socketListen();
    }

    getApp(): express.Application {
        return this.app;
    }

    initConfigServer(): void {
        this.app = express();
        this.server = createServer(this.app);
        this.port = process.env.PORT || Constant.PORT_SUM_SERVER;
        this.io = SocketIO(this.server);
    }

    socketListen(): void {
        this.server.listen(this.port, () => {
        });

        this.io.on(Event.CONNECT, () => {
            setInterval(() => {
                const numberOne: number = SumCore.GenerateRandomNumberInt();
                const numberTwo: number = SumCore.GenerateRandomNumberInt();
                const resultSum: number = SumCore.SumNumbers(numberOne, numberTwo);

                this.io.emit(Event.SUM_VALID_NUMBER, resultSum);
            }, 1000);
        });
    }
}
