/**
 * Aca esta todos los Events que queramos transmitir en el Web Socket.
 */
export enum Event {
    CONNECT = 'connect',
    DISCONNECT = 'disconnect',
    SUM_VALID_NUMBER = 'sumValidNumbers',
    SUM_RESULT = 'sumResult',
    JOIN_NEW_USER = 'joinNewUser',
    LEAVE_USER = 'leaveUser'
}

