import {ServerSum} from "./serverSum";
import {ServerSumResult} from "./serverSumResult";

const appSum = new ServerSum().getApp();
const appSumResult = new ServerSumResult().getApp();
export {appSum, appSumResult};
