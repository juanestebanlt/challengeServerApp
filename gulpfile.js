let gulp = require("gulp");
let ts = require("gulp-typescript");
let sourcemaps = require('gulp-sourcemaps');

let tsProject = ts.createProject("tsconfig.json", { rootDir: "./src" });


gulp.task('default', ['build']);

gulp.task("build", ["typescript", "watch"]);

gulp.task("typescript", function () {
    return tsProject.src()
        .pipe(sourcemaps.init()) // This means sourcemaps will be generated
        .pipe(tsProject())
        .js
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("./dist"));
});

gulp.task("watch", function () {
    gulp.watch('./src/**/*.ts', ['typescript']);
});

let onError = function (err) {
    console.log(err);
}
