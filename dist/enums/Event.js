"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Aca esta todos los Events que queramos transmitir en el Web Socket.
 */
var Event;
(function (Event) {
    Event["CONNECT"] = "connect";
    Event["SUM_VALID_NUMBER"] = "sumValidNumbers";
})(Event = exports.Event || (exports.Event = {}));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9lbnVtcy9FdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBOztHQUVHO0FBQ0gsSUFBWSxLQUdYO0FBSEQsV0FBWSxLQUFLO0lBQ2IsNEJBQW1CLENBQUE7SUFDbkIsNkNBQW9DLENBQUE7QUFDeEMsQ0FBQyxFQUhXLEtBQUssR0FBTCxhQUFLLEtBQUwsYUFBSyxRQUdoQiIsImZpbGUiOiJlbnVtcy9FdmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBBY2EgZXN0YSB0b2RvcyBsb3MgRXZlbnRzIHF1ZSBxdWVyYW1vcyB0cmFuc21pdGlyIGVuIGVsIFdlYiBTb2NrZXQuXHJcbiAqL1xyXG5leHBvcnQgZW51bSBFdmVudCB7XHJcbiAgICBDT05ORUNUID0gJ2Nvbm5lY3QnLFxyXG4gICAgU1VNX1ZBTElEX05VTUJFUiA9ICdzdW1WYWxpZE51bWJlcnMnXHJcbn1cclxuXHJcbiJdfQ==
