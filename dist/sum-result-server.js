"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const http_1 = require("http");
const SocketIO = require("socket.io");
const ioClient = require("socket.io-client");
const os = require("os");
class SumResultServer {
    constructor() {
        this.Init();
        this.SocketListen();
    }
    /**
     * Inicializamos todas las configuraciones necesarias
     * para que el socket pueda funcionar de manera correcta
     * @constructor
     */
    Init() {
        this.app = express();
        this.server = http_1.createServer(this.app);
        this.port = process.env.PORT || SumResultServer.PORT;
        this.io = SocketIO(this.server);
    }
    /**
     * Configuramos el socket para que escuche las peticiones del puerto,
     * {@link port}.
     * @constructor
     */
    SocketListen() {
        this.server.listen(this.port, () => {
            console.log(`Server Run on port ${this.port}`);
        });
        this.io.on("connect", (socket) => {
            console.log(`client connected ${this.port}`);
            /*socket.on("message", (m: any) => {
                console.log(`Server Message ${JSON.stringify(m)}`);
                this.io.emit("message", m);
            });*/
            socket.on("disconnect", () => {
                console.log("Client disconnected");
            });
        });
        let socketCli = ioClient(`http://${os.hostname()}:27877`);
        socketCli.on("connect", () => {
            console.log(`connect client`);
            socketCli.on("sum-valid-numbers", (valor) => {
                this.io.emit("sum-result", valor);
            });
        });
    }
    getAppSumResultServer() {
        return this.app;
    }
}
SumResultServer.PORT = 27878;
exports.SumResultServer = SumResultServer;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9zdW0tcmVzdWx0LXNlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1DQUFtQztBQUNuQywrQkFBMEM7QUFDMUMsc0NBQXNDO0FBQ3RDLDZDQUE2QztBQUM3Qyx5QkFBeUI7QUFFekI7SUFPSTtRQUNJLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNaLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLElBQUk7UUFDUixJQUFJLENBQUMsR0FBRyxHQUFHLE9BQU8sRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsbUJBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDO1FBQ3JELElBQUksQ0FBQyxFQUFFLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLFlBQVk7UUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUU7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUE7UUFDbEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxNQUFXLEVBQUUsRUFBRTtZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUU3Qzs7O2lCQUdLO1lBRUwsTUFBTSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUUsR0FBRyxFQUFFO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksU0FBUyxHQUFHLFFBQVEsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDMUQsU0FBUyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsR0FBRyxFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUM5QixTQUFTLENBQUMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLENBQUMsS0FBVSxFQUFFLEVBQUU7Z0JBQzdDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLHFCQUFxQjtRQUN4QixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDcEIsQ0FBQzs7QUF6RGUsb0JBQUksR0FBVyxLQUFLLENBQUM7QUFEekMsMENBMkRDIiwiZmlsZSI6InN1bS1yZXN1bHQtc2VydmVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xyXG5pbXBvcnQge2NyZWF0ZVNlcnZlciwgU2VydmVyfSBmcm9tIFwiaHR0cFwiO1xyXG5pbXBvcnQgKiBhcyBTb2NrZXRJTyBmcm9tIFwic29ja2V0LmlvXCI7XHJcbmltcG9ydCAqIGFzIGlvQ2xpZW50IGZyb20gJ3NvY2tldC5pby1jbGllbnQnO1xyXG5pbXBvcnQgKiBhcyBvcyBmcm9tICdvcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgU3VtUmVzdWx0U2VydmVyIHtcclxuICAgIHN0YXRpYyByZWFkb25seSBQT1JUOiBudW1iZXIgPSAyNzg3ODtcclxuICAgIHByaXZhdGUgYXBwOiBleHByZXNzLkFwcGxpY2F0aW9uO1xyXG4gICAgcHJpdmF0ZSBzZXJ2ZXI6IFNlcnZlcjtcclxuICAgIHByaXZhdGUgaW86IFNvY2tldElPLlNlcnZlcjtcclxuICAgIHByaXZhdGUgcG9ydDogc3RyaW5nIHwgbnVtYmVyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuSW5pdCgpO1xyXG4gICAgICAgIHRoaXMuU29ja2V0TGlzdGVuKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbmljaWFsaXphbW9zIHRvZGFzIGxhcyBjb25maWd1cmFjaW9uZXMgbmVjZXNhcmlhc1xyXG4gICAgICogcGFyYSBxdWUgZWwgc29ja2V0IHB1ZWRhIGZ1bmNpb25hciBkZSBtYW5lcmEgY29ycmVjdGFcclxuICAgICAqIEBjb25zdHJ1Y3RvclxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIEluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hcHAgPSBleHByZXNzKCk7XHJcbiAgICAgICAgdGhpcy5zZXJ2ZXIgPSBjcmVhdGVTZXJ2ZXIodGhpcy5hcHApO1xyXG4gICAgICAgIHRoaXMucG9ydCA9IHByb2Nlc3MuZW52LlBPUlQgfHwgU3VtUmVzdWx0U2VydmVyLlBPUlQ7XHJcbiAgICAgICAgdGhpcy5pbyA9IFNvY2tldElPKHRoaXMuc2VydmVyKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbmZpZ3VyYW1vcyBlbCBzb2NrZXQgcGFyYSBxdWUgZXNjdWNoZSBsYXMgcGV0aWNpb25lcyBkZWwgcHVlcnRvLFxyXG4gICAgICoge0BsaW5rIHBvcnR9LlxyXG4gICAgICogQGNvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgU29ja2V0TGlzdGVuKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuc2VydmVyLmxpc3Rlbih0aGlzLnBvcnQsICgpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYFNlcnZlciBSdW4gb24gcG9ydCAke3RoaXMucG9ydH1gKVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmlvLm9uKFwiY29ubmVjdFwiLCAoc29ja2V0OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYGNsaWVudCBjb25uZWN0ZWQgJHt0aGlzLnBvcnR9YCk7XHJcblxyXG4gICAgICAgICAgICAvKnNvY2tldC5vbihcIm1lc3NhZ2VcIiwgKG06IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coYFNlcnZlciBNZXNzYWdlICR7SlNPTi5zdHJpbmdpZnkobSl9YCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlvLmVtaXQoXCJtZXNzYWdlXCIsIG0pO1xyXG4gICAgICAgICAgICB9KTsqL1xyXG5cclxuICAgICAgICAgICAgc29ja2V0Lm9uKFwiZGlzY29ubmVjdFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkNsaWVudCBkaXNjb25uZWN0ZWRcIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgc29ja2V0Q2xpID0gaW9DbGllbnQoYGh0dHA6Ly8ke29zLmhvc3RuYW1lKCl9OjI3ODc3YCk7XHJcbiAgICAgICAgc29ja2V0Q2xpLm9uKFwiY29ubmVjdFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGBjb25uZWN0IGNsaWVudGApO1xyXG4gICAgICAgICAgICBzb2NrZXRDbGkub24oXCJzdW0tdmFsaWQtbnVtYmVyc1wiLCAodmFsb3I6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pby5lbWl0KFwic3VtLXJlc3VsdFwiLCB2YWxvcik7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEFwcFN1bVJlc3VsdFNlcnZlcigpOiBleHByZXNzLkFwcGxpY2F0aW9uIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHA7XHJcbiAgICB9XHJcbn1cclxuIl19
