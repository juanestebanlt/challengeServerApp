"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SumCore {
    static GenerateRandomNumberInt(minNumber = 0, maxNumber = 20) {
        const randomNumber = Math.random() * (maxNumber - minNumber) + minNumber;
        return Math.round(Math.floor(randomNumber));
    }
    static SumNumbers(numberOne, numberTwo) {
        if (SumCore.ValidIncomingData(numberOne, numberTwo)) {
            return numberOne + numberTwo;
        }
        return 0;
    }
    static ValidIncomingData(numberOne, numberTwo) {
        return (numberOne >= 0 && numberOne <= 20)
            && (numberTwo >= 0 && numberTwo <= 20);
    }
}
exports.SumCore = SumCore;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jb3JlL3N1bS1jb3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUE7SUFDSSxNQUFNLENBQUMsdUJBQXVCLENBQUMsWUFBb0IsQ0FBQyxFQUFFLFlBQW9CLEVBQUU7UUFDeEUsTUFBTSxZQUFZLEdBQVcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxHQUFHLFNBQVMsQ0FBQztRQUNqRixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxNQUFNLENBQUMsVUFBVSxDQUFDLFNBQWlCLEVBQUUsU0FBaUI7UUFDbEQsSUFBSSxPQUFPLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxFQUFFO1lBQ2pELE9BQU8sU0FBUyxHQUFHLFNBQVMsQ0FBQztTQUNoQztRQUNELE9BQU8sQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFpQixFQUFFLFNBQWlCO1FBQ3pELE9BQU8sQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsSUFBSSxFQUFFLENBQUM7ZUFDbkMsQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUUvQyxDQUFDO0NBQ0o7QUFsQkQsMEJBa0JDIiwiZmlsZSI6ImNvcmUvc3VtLWNvcmUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgU3VtQ29yZSB7XHJcbiAgICBzdGF0aWMgR2VuZXJhdGVSYW5kb21OdW1iZXJJbnQobWluTnVtYmVyOiBudW1iZXIgPSAwLCBtYXhOdW1iZXI6IG51bWJlciA9IDIwKTogbnVtYmVyIHtcclxuICAgICAgICBjb25zdCByYW5kb21OdW1iZXI6IG51bWJlciA9IE1hdGgucmFuZG9tKCkgKiAobWF4TnVtYmVyIC0gbWluTnVtYmVyKSArIG1pbk51bWJlcjtcclxuICAgICAgICByZXR1cm4gTWF0aC5yb3VuZChNYXRoLmZsb29yKHJhbmRvbU51bWJlcikpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBTdW1OdW1iZXJzKG51bWJlck9uZTogbnVtYmVyLCBudW1iZXJUd286IG51bWJlcik6IG51bWJlciB7XHJcbiAgICAgICAgaWYgKFN1bUNvcmUuVmFsaWRJbmNvbWluZ0RhdGEobnVtYmVyT25lLCBudW1iZXJUd28pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudW1iZXJPbmUgKyBudW1iZXJUd287XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBWYWxpZEluY29taW5nRGF0YShudW1iZXJPbmU6IG51bWJlciwgbnVtYmVyVHdvOiBudW1iZXIpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gKG51bWJlck9uZSA+PSAwICYmIG51bWJlck9uZSA8PSAyMClcclxuICAgICAgICAgICAgJiYgKG51bWJlclR3byA+PSAwICYmIG51bWJlclR3byA8PSAyMCk7XHJcblxyXG4gICAgfVxyXG59XHJcbiJdfQ==
