import {Event} from "../src/enums/Event";
import * as os from "os";
import * as socketIOClient from "socket.io-client";
import {Constant} from "../src/helper/constant";
import {isNumber} from "util";

describe('Debe devolver un numero aleatorio', () => {
    it("Debe ser un numero", (done) => {
        let ioClient = socketIOClient(`http://${os.hostname()}:${Constant.PORT_SUM_SERVER}`);
        ioClient.on(Event.CONNECT, () => {
            ioClient.on(Event.SUM_VALID_NUMBER, (sumValue: number) => {
                if (isNumber(sumValue)) {
                    done();
                }
            });
        });
    });
});
